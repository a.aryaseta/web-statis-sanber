<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){

        return view('register.register');

    }

    public function kirim(Request $request){

        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];
        $gender = $request['gender'];
        $kebangsaan = $request['kebangsaan'];
        $language = $request['language'];
        $bio = $request['bio'];

        return view('welcome.welcome', ['namaDepan'=>$namaDepan, 'namaBelakang'=>$namaBelakang, 'gender'=>$gender, 'kebangsaan'=>$kebangsaan, 'language'=>$language, 'bio'=>$bio,]);

    }
}
