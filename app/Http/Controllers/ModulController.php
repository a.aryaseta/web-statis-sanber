<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ModulController extends Controller
{
    public function index(){

        return view('home.home');

    }

    public function table(){

        return view('table.table');

    }

    public function datatable(){

        return view('data-table.data-table');

    }
}
