<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.index',['cast' => $cast]);
    }

    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' =>'required',
            'umur' =>'required|integer',
            'bio' =>'required'
        ],
        [
            'nama.required' => 'nama tidak boleh kosong',
            'umur.required' => 'umur tidak boleh kosong',
            'umur.integer' => 'umur harus diisi dengan angka',
            'bio.required' => 'bio tidak boleh kosong'
        ]);

        DB::table('cast')->insert(
            [
                'nama' => $request->nama,
                'umur' => $request->umur,
                'bio' => $request->bio,
            ]
        );
        return redirect('/cast');
    }

    public function show($id){
        $cast = DB::table('cast')->find($id);
        return view('cast.detail',['cast' => $cast]);
    }

    public function edit($id){
        $cast = DB::table('cast')->find($id);
        return view('cast.edit',['cast' => $cast]);
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' =>'required',
            'umur' =>'required|integer',
            'bio' =>'required'
        ],
        [
            'nama.required' => 'nama tidak boleh kosong',
            'umur.required' => 'umur tidak boleh kosong',
            'umur.integer' => 'umur harus diisi dengan angka',
            'bio.required' => 'bio tidak boleh kosong'
        ]);

        $affected = DB::table('cast')
              ->where('id', $id)
              ->update([
                'nama' => $request->nama,
                'umur' => $request->umur,
                'bio' => $request->bio,
            ]);
        return redirect('/cast');
    }

    public function destroy($id){
        $cast = DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
