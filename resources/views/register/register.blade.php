@extends('layouts.app')

@section('css')
@include('register.css')
@endsection

@section('content')
<h1>Buat Account Baru!</h1><br>
<h4>Sign Up Form</h4><br>
<div class="containerFormSanberCode">
    <form action="/welcome" method="post">
        @csrf
        <label for="fname">First name:</label><br>
        <input type="text" id="fname" name="fname"><br>
        <label for="lname">Last name:</label><br>
        <input type="text" id="lname" name="lname"><br><br>
        <label for="gender">Jenis-kelamin:</label><br>
        <input type="radio" id="pria" name="gender" value="pria">
        <label for="pria">Pria</label><br>
        <input type="radio" id="perempuan" name="gender" value="perempuan">
        <label for="perempuan">Perempuan</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>
        <label for="kebangsaan">Kebangsaan:</label><br>
        <select name="kebangsaan" id="kebangsaan">
                <option value="indonesian">Indonesian</option>
                <option value="singapore">Singaporean</option>
                <option value="malaysian">Malaysian</option>
                <option value="australian">Australian</option>
        </select><br><br>
        <label for="language-spoken">Bahasa Sehari-hari</label><br>
        <input type="checkbox" id="language" name="language" value="bahasa indonesia">
        <label for="language">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="english" value="english">
        <label for="english">English</label><br>
        <input type="checkbox" id="other" name="other" value="other">
        <label for="other">Other</label><br><br>
        <label for="language-spoken">Bio:</label><br>
        <textarea id="bio" name="bio" rows="4" cols="21">
        </textarea><br><br>

        <button class="button" type="submit">Submit</button>
    </form>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script type="text/javascript">
$(function(){

	$('button[type=submit]').on('click', function(){
		$('form').attr('action', '/welcome');
	})

});
</script>
@endsection
