@extends('layouts.master')

@section('title')
    Halaman Edit Cast
@endsection

@section('content')
<div class="card" style="border:none;">
  <div class="card-body">
    <div class="col-12 p-0">
      <form action="/cast/{{$cast->id}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" id="nama" name="nama" value="{{$cast->nama}}">
        </div>
        @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label for="umur">Umur</label>
          <input type="text" class="form-control" id="umur" name="umur" value="{{$cast->umur}}">
        </div>
        @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label for="bio">Biodata</label>
          <textarea class="form-control" id="bio" name="bio" rows="3">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-success">Simpan</button>
            <a href="/cast" class="btn btn-warning text-white">Batal</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection