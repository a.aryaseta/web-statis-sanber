@extends('layouts.master')

@section('title')
  Detail Data Cast
@endsection

@section('sub-title')
	<a href="/cast" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
@endsection

@section('content')
<div class="card" style="border:none;">
    <div class="card-body">
        <div class="col-12 p-0">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">Umur</th>
                        <th scope="col">Biodata</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$cast->nama}}</td>
                        <td>{{$cast->umur}}</td>
                        <td>{{$cast->bio}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection