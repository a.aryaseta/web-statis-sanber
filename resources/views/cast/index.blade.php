@extends('layouts.master')

@section('title')
  Data Cast
@endsection

@section('sub-title')
	<a href="/cast/create" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Data</a>
@endsection

@section('content')
<div class="card" style="border:none;">
  <div class="card-body">
        <div class="col-12 p-0">
            <table class="table">
                <thead class="thead-dark">
                    <tr class="text-center">
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($cast as $key => $cast)
                        <tr class="text-center">
                            <td>{{$key + 1}}</td>
                            <td>{{$cast->nama}}</td>
                            <td>
                            <form action="/cast/{{$cast->id}}" method="post">
                                @csrf
                                @method('delete')
                                <a href="/cast/{{$cast->id}}" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></a>
                                <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm text-white"><i class="fa fa-pen"></i></a>
                                <button onclick="return confirm('Yakin mau di hapus?')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                            </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>Belum ada data</td>
                        </tr>
                    @endforelse 
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection