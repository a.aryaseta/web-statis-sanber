<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright Aulia Aryaseta &copy; 2022 All rights reserved <a href="https://gitlab.com/a.aryaseta">Gitlab Here</a>.</strong>
</footer>