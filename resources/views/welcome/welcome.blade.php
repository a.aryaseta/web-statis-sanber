@extends('layouts.app')

@section('css')
@include('welcome.css')
@endsection

@section('content')
<h1>Selamat datang {{$namaDepan}} {{$namaBelakang}} !</h1><br>
<h3>Berikut Biodata kamu :</h3>
<h5>Nama Depan : {{$namaDepan}}</h5>
<h5>Nama Belakang : {{$namaBelakang}}</h5>
<h5>Jenis Kelamin : {{$gender}}</h5>
<h5>Kebangsaan : {{$kebangsaan}}</h5>
<h5>Bahasa : {{$language}}</h5>
<h5>Biodata : {{$bio}}</h5>
<p>Klik <a href="/">disini</a> untuk kembali ke halaman awal</p>
@endsection